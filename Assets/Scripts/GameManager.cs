﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public int score;
    public const int PICKUP_POINTS = 100;

	public static GameManager instance;

	private void Awake () {
		if (instance == null) {			
			instance = this;
		} else if (instance != null) {
			Destroy (gameObject);
		}
		DontDestroyOnLoad (gameObject);
	}

	void Start () {
		// ...
	}	

	void Update () {
		// ...
	}

	public void StartGame () {
		GUIManager.instance.hudMenu.SetActive(false);
        Debug.Log ("StartGame!");
    }
    public void SoundOff() {
        GUIManager.instance.buttonSound.SetActive(false);
        Debug.Log("Sound off!");
    }
    public void About() {
        GUIManager.instance.buttonAbout.SetActive(false);
        Debug.Log("About!");
    }

    public void SetScore(int newScore) {
        score = newScore;
    }

    public int GetScore() {
        return score;
    }

	public void ResetGame() {
		UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelMover : MonoBehaviour {

    [SerializeField]
    float stepSize = 1f;
    [SerializeField]
    float stepTime = 0.3f;
    float turnTime = 0.2f;
    bool isMoving;
    float gameSpeed;
    float runSpeed = 4.0f;


    /// <summary>
    /// Moves the world GameObject
    /// </summary>
    public void Move (float speed) {
        gameSpeed = speed;
        transform.position = new Vector3(
            transform.position.x,
            transform.position.y,
            transform.position.z - Time.deltaTime * runSpeed * gameSpeed);
    }


    public void Input (float input) {
        //if (Input.GetKeyUp(KeyCode.Z)) {
        //    LateralStep(1.0f);
        //}
        //if (Input.GetKeyUp(KeyCode.X)) {
        //    LateralStep(-1.0f);
        //}
        //if (Input.GetKeyUp(KeyCode.S)) {
        //    Rotate(-90.0f);
        //}
        //if (Input.GetKeyUp(KeyCode.A)) {
        //    Rotate(90.0f);
        //}
    }



    public void Rotate (float degrees) {
        if (!isMoving) {
            //Debug.Log(turnTime / Time.deltaTime + " " + 60* turnTime / gameSpeed );
            StartCoroutine(LinearTurn(degrees, turnTime / gameSpeed));
        }
    }


    protected IEnumerator LinearTurn (float degrees, float duration) {
        //float newX = Mathf.Round(transform.position.z);
        //float newZ = Mathf.Round(transform.position.x) * direction;
        //Vector3 destination = new Vector3(newX, 0.0f, newZ);
        //Debug.Log("start pos: " + transform.position + " to " + destination);
        Vector3 endPoint = Quaternion.Euler(0, degrees , 0) * transform.position;
        Debug.Log(transform.position + " to " + endPoint);

        isMoving = true;
        float aggregate = 0; // total
        while (aggregate < 1f) {
            aggregate += Time.deltaTime / duration;
            /* 
             * deltaTime = 0.0170017
             * turnTime / deltaTime = numFrames
            */
            float rotationAmount = Mathf.LerpAngle(0f, degrees, aggregate);
            //Debug.Log(rotationAmount);
            rotationAmount = degrees / (turnTime / Time.deltaTime);
            //Debug.Log(duration + " & " + aggregate + " & " + rotationAmount);
            //transform.parent.transform.eulerAngles = Vector3.Lerp(transform.parent.transform.rotation.eulerAngles, Vector3.up, rotationAmount);
            //transform.parent.transform.eulerAngles = new Vector3(0.0f, rotationAmount, 0.0f);
            //Quaternion target = Quaternion.Euler(0, 90, 0);
            //transform.rotation = Quaternion.Slerp(transform.rotation, target, aggregate);
            //http://answers.unity3d.com/questions/664732/i-need-to-set-rotation-around-arbitrary-point.html
            transform.RotateAround(Vector3.zero, Vector3.up, rotationAmount);
            //transform.localEulerAngles = new Vector3(0.0f, rotationAmount, 0.0f);
            yield return null;
        }
        //Debug.Log(aggregate);
        isMoving = false;

        float endAngle = transform.eulerAngles.y;
        endAngle = Mathf.Round(endAngle / 90) * 90;
        //Debug.Log("end turn degree: " + transform.eulerAngles.y + " rounded to: " + endAngle);
        transform.eulerAngles = new Vector3(0.0f, endAngle, 0.0f);
        transform.position = endPoint;
    }

    //public Vector3 RotatePointAroundPivot (Vector3 point, Vector3 pivot, Vector3 angles) {
    //    return Quaternion.Euler(angles) * (point - pivot) + pivot;
    //}


    /// <summary>
    /// Moves one meter based on direction
    /// </summary>
    public void LateralStep (float direction) {
        if (!isMoving) {
            StartCoroutine(LinearMovement(transform.position.x, transform.position.x + direction * stepSize, stepTime / gameSpeed));
        }
    }


    protected IEnumerator LinearMovement (float from, float to, float duration) {
        //if (duration < float.Epsilon) {
        //    transform.position = new Vector3( to, transform.position.y, transform.position.z);
        //    yield break;
        //}

        isMoving = true;
        float aggregate = 0;
        while (aggregate < 1f) {
            aggregate += Time.deltaTime / duration;
            transform.position = new Vector3(Mathf.Lerp(from, to, aggregate), transform.position.y, transform.position.z);
            yield return null;
        }
        isMoving = false;
    }



}

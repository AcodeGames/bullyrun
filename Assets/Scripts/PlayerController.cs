﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.UI;

// This is heavily modified version of ThirdPersonUserControl.cs script originally at:
// Assets/Standard Assets/Characters/ThirdPersonCharacter/Scripts/ThirdPersonUserControl.cs
// Modified to do endless runner mechanics

namespace UnityStandardAssets.Characters.ThirdPerson
{

	[RequireComponent (typeof(PlayerCharacter))]

	public class PlayerController : MonoBehaviour
	{

		[Range (0.001f, 2.0f)]
		public float gameSpeed = 1.0f;

		private float lateralForce = 40f;
		private PlayerCharacter character;
		private bool jump;
		private Rigidbody rb;
		private Animator animator;
		private float lateralMovement;
		private bool grounded;

		private GameObject world;
		private LevelMover levelMover;

		private bool inCorner = false;

		private Text gameScoreText;

        AudioSource audioPickup;

        private AudioSource[] allAudioSources;

        private void Start ()
		{
            allAudioSources = GetComponents<AudioSource>();
            // get the transform of the main camera
            if (Camera.main != null) {
			//m_Cam = Camera.main.transform;
			} else {
				Debug.LogWarning (
					"Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.", gameObject);
				// we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
			}

			gameScoreText = GameObject.Find ("Canvas/HUD - Gameplay/Text").GetComponent<Text> ();
			world = GameObject.Find ("World");
			levelMover = world.GetComponent<LevelMover> ();

			character = GetComponent<PlayerCharacter> ();
			animator = GetComponent<Animator> ();
			rb = GetComponent<Rigidbody> ();

			gameScoreText.text = GameManager.instance.GetScore().ToString();
		}


		// Fixed update is called 50fps in sync with physics. Don't use Input checking in FixedUpdate!
		private void FixedUpdate ()
		{

			grounded = CheckGround ();

			if (!grounded) {
				return;
			}

			// NB! sideways movement screws up while in air
			if (lateralMovement != 0) {
				rb.AddForce (transform.right * lateralMovement * lateralForce, ForceMode.Impulse);
			}

			if (jump) {
				//rb.MovePosition(transform.position + new Vector3(0.0f, 0.0f, 0.1f));
				rb.AddForce (new Vector3 (0, 200, 0), ForceMode.Impulse);
				jump = false;
			}

			//rb.velocity = new Vector3(0.0f, rb.velocity.y, 2.0f);
			//rb.AddForce(movement, ForceMode.Impulse);

		}


		private void Update ()
		{

			// world doesnt stop
			UpdateWorld ();
			UpdateAnimator ();

			// Reading input onoly when grounded
			if (!grounded) {
				return;
			}

			if (!jump) {
				jump = CrossPlatformInputManager.GetButtonDown ("Jump") || SwipeManager.IsSwipingUp ();
			}

			//lateralMovement = Input.GetAxis("Horizontal");
			//levelMover.Input(lateralMovement);

			// HACK - to be refactored into states
			// left input
			if (Input.GetKeyUp (KeyCode.LeftArrow) || SwipeManager.IsSwipingLeft ()) {
				//levelMover.Input(-1.0f);
				if (inCorner) {
					inCorner = false;
					Debug.Log (inCorner);
					levelMover.Rotate (90.0f);
				} else {
					levelMover.LateralStep (1.0f);
				}
			}
			// right input
			if (Input.GetKeyUp (KeyCode.RightArrow) || SwipeManager.IsSwipingRight ()) {
				//levelMover.Input(1.0f);
				if (inCorner) {
					inCorner = false;
					Debug.Log (inCorner);
					levelMover.Rotate (-90.0f);
				} else {
					levelMover.LateralStep (-1.0f);
				}
			}
		}

		private void UpdateWorld ()
		{

			levelMover.Move (gameSpeed);
		}

		private void UpdateAnimator ()
		{

			animator.speed = gameSpeed;
			animator.SetBool ("OnGround", grounded);

			// calculate which leg is behind, so as to leave that leg trailing in the jump animation
			// (This code is reliant on the specific run cycle offset in our animations,
			// and assumes one leg passes the other at the normalized clip times of 0.0 and 0.5)
			float runCycle =
				Mathf.Repeat (
					animator.GetCurrentAnimatorStateInfo (0).normalizedTime + 0.2f, 1);
			//float jumpLeg = (runCycle < 0.5f ? 1 : -1) * movement.z;
			float jumpLeg = (runCycle < 0.5f ? 1 : -1) * 0.1f;
			if (grounded) {
				animator.SetFloat ("JumpLeg", jumpLeg);
			}
		}


		//private void turnPlayer (float hInput) {
		//    hInput = Mathf.Ceil(hInput) * 2 - 1;    // hInput is now 1 || -1
		//    movement = Quaternion.Euler(0, 90 * hInput, 0) * movement;
		//}


		private bool CheckGround ()
		{

			RaycastHit hitInfo;
			float distance = 0.3f;

#if UNITY_EDITOR
			// helper to visualise the ground check ray in the scene view
			Debug.DrawLine (transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * 0.1f), Color.blue);
			Debug.DrawLine (transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.down * distance), Color.red);
#endif

			// current solution may not be the best
			if (Physics.Raycast (transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, distance)) {
				return true;
			} else {
				return false;
			}
		}


		private void OnCollisionEnter (Collision collision)
		{
			if (collision.gameObject.tag == "Wall") {
				Debug.Log ("Death");               

                // Show a question to watch a rewarded ad in exchange of restarting the game with the same amount of points.
                // Request rewarded ad from unity.
                AdsManager.instance.ShowRewardedAd();               
			}

			if (collision.gameObject.tag == "pickUpItem") {
				Debug.Log ("pickUpItem");
				GameManager.instance.SetScore (GameManager.instance.GetScore() + GameManager.PICKUP_POINTS);
                audioPickup = allAudioSources[0];

                audioPickup.Play();
				gameScoreText.text = GameManager.instance.GetScore().ToString();

				Destroy (collision.gameObject);
			}
		}


		private void OnTriggerEnter (Collider other)
		{

			if (other.name == "CornerTrigger") {
				inCorner = true;
				Debug.Log ("Entered into corner!");
			}
		}

		private void OnTriggerExit (Collider other)
		{

			if (other.name == "CornerTrigger") {
				inCorner = false;
				Debug.Log ("Exited corner!");
			}
		}

	}
}
﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Advertisements;

public class AdsManager : MonoBehaviour {

    public static AdsManager instance;

    private void Awake() {
        if (instance == null) {
            instance = this;
        } else if (instance != null) {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    public void ShowRewardedAd() {
		if (Advertisement.IsReady ("rewardedVideo")) {
			ShowOptions options = new ShowOptions { resultCallback = HandleShowResult };
			Advertisement.Show ("rewardedVideo", options);
		} else {
			Debug.Log ("Ads are not working!");
			GameManager.instance.ResetGame ();
		}
    }

    private void HandleShowResult(ShowResult result) {
        result = ShowResult.Failed;
        if (result != ShowResult.Finished) {
            GameManager.instance.SetScore(0);
        }

		GameManager.instance.ResetGame ();        
    }

}

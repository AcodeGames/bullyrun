﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepOnTrigger : MonoBehaviour {

    private LevelManager levelManager;

    // Use this for initialization
    void Start () {

        levelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
    }

    void OnTriggerEnter (Collider Other) {

        levelManager.AddFloor();
        //Debug.Log("new floor triggered at " + transform);
    }

    private void OnTriggerExit (Collider other) {

        levelManager.RemoveFloorTile(this.transform.parent.gameObject);
    }
}

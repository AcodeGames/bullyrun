﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {

	public GameObject[] pickUpItems;
    public GameObject[] obstacles;
    public GameObject[] floorTiles;
    public bool firstTileOnly;
    public bool jumpCamEnabled;

    private List<GameObject> floorList = new List<GameObject>();
    private GameObject world;
    private GameObject newFloor;
    private GameObject newObstacle;
	private GameObject newPickUpItem;
	private int floorCounter;

    AudioSource audioBell;

    // Use this for initialization
    void Start () {
        AudioSource[] allAudioSources = GetComponents<AudioSource>();
        audioBell = allAudioSources[0];

        audioBell.Play();

        if (jumpCamEnabled) {
            GameObject pivot = GameObject.Find("MultipurposeCameraRig/Pivot");
            pivot.transform.localPosition = new Vector3(-1.0f, 1.0f, -1.0f);
            pivot.transform.eulerAngles = new Vector3(20, 40, 0);
        }

        world = GameObject.Find("World");

		floorCounter = 0;

        AddFloor(0);
        AddFloor(0);
        AddFloor(0);
        //AddFloor(3);
        //AddFloor(0);
        //AddFloor(0);
        //AddFloor(3);
        //AddFloor(0);
        //AddFloor(0);
        //AddFloor(0);
        //AddFloor(1);
        //AddFloor(0);
        //AddFloor(0);
        //AddFloor(1);
        AddFloor(0);
    }

    public void AddFloor () {
        int floorTileType = firstTileOnly ? 0 : Random.Range(0, floorTiles.Length);
        AddFloor(floorTileType);
    }

    public void AddFloor (int id) {

        Transform refPoint;
        int lastFloor = floorList.Count - 1;
        // get and use last floor RefPoint parameters
        if (lastFloor >= 0) {
            refPoint = floorList[lastFloor].transform.FindChild("RefPoint").transform;
        }
        else {
            refPoint = GameObject.Find("StartTile").transform.FindChild("RefPoint").transform;
        }

        newFloor = Instantiate(floorTiles[id]);
        newFloor.transform.position = refPoint.position;
        newFloor.transform.rotation = refPoint.rotation;
        newFloor.transform.parent = world.transform;

        int spawnPointId = Random.Range(0, 4);

        //if (spawnPointId > 0 && id % 2 == 0) {
        //    Transform spawnPoint = newFloor.transform.FindChild("spawnPoint" + spawnPointId).transform;
        //    newObstacle = Instantiate(obstacles[Random.Range(0, obstacles.Length)]);
        //    newObstacle.transform.position = spawnPoint.position + new Vector3(0.0f, 0.33f / 2, 0.0f);
        //    newObstacle.transform.parent = newFloor.transform;
        //}

		if (spawnPointId > 0 && id % 2 == 0) {
			Transform spawnPoint = newFloor.transform.FindChild("spawnPoint" + spawnPointId).transform;
			newPickUpItem = Instantiate(pickUpItems[Random.Range(0, pickUpItems.Length)]);
			newPickUpItem.transform.position = spawnPoint.position + new Vector3(0.0f, 0.33f / 2, 0.0f);
			newPickUpItem.transform.parent = newFloor.transform;
		}

        if (CheckLastDoubleTurns(newFloor.name)) {
            Destroy(newFloor);
            AddFloor();
        }
        else {
            floorList.Add(newFloor);
        }
        
    }


    public void RemoveFloorTile (GameObject tile) {

        Destroy(tile, 2);
        floorList.Remove(tile);
		floorCounter++;
    }

    /// <summary>
    /// Moves the world GameObject
    /// </summary>
    public void Move (float gameSpeed) {
        world.transform.position = new Vector3(
            world.transform.position.x,
            world.transform.position.y,
            world.transform.position.z - Time.deltaTime * 5.0f * gameSpeed);
    }

    // very bad temp code
    private bool CheckLastDoubleTurns (string newTurn) {
        string output = "";
        List<string> turns = new List<string>();
        foreach (GameObject tile in floorList) {
            if (tile.name == "Hall1(Clone)" || tile.name == "Hall2(Clone)" || tile.name == "Hall3(Clone)") {
                continue;
            }
            turns.Add(tile.name);
            output += tile.name;
        }
        turns.Reverse();

        if (turns.Count > 1 && newTurn == turns[0] && newTurn == turns[1]) {
            return true;
        }
        return false;
    }

}

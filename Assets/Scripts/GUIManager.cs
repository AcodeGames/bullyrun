﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIManager : MonoBehaviour {

	public static GUIManager instance;

	[Header ("HUD - Menu")]
	public GameObject hudSplash;

	[Header ("HUD - Menu")]
	public GameObject hudMenu;
	public GameObject buttonPlay;
	public GameObject buttonSound;
    public GameObject buttonAbout;

	[Header ("HUD - Gameplay")]
	public GameObject hudGameplay;
	public GameObject buttonPause;

	[Header ("HUD - Result")]
	public GameObject hudResult;
	public GameObject buttonVideo;
	public GameObject buttonRetry;

	[Header ("HUD - About")]
	public GameObject hudAbout;
	public GameObject buttonBack;

	private void Awake () {
		if (instance == null) {			
			instance = this;
		} else if (instance != null) {
			Destroy (gameObject);
		}
		DontDestroyOnLoad (gameObject);
	}
		
	void Start () {
        //HUD - Menu
		buttonPlay.GetComponent<Button> ().onClick.AddListener (() => GameManager.instance.StartGame ());
        buttonSound.GetComponent<Button>().onClick.AddListener(() => GameManager.instance.SoundOff());
        buttonAbout.GetComponent<Button>().onClick.AddListener(() => GameManager.instance.About());


    }

}